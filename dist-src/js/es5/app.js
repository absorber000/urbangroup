"use strict";

;(function (win, doc) {
    'use strict';

    function carousels() {
        var projectSlider = $("#projects-slider");
        var galleryCarousel = $("#gallery-slider");
        var galleryPrev = $("#gallery-slider-prev");
        var galleryNext = $("#gallery-slider-next");
        galleryPrev.click(function () {
            galleryCarousel.trigger('prev.owl.carousel');
        });
        galleryNext.click(function () {
            galleryCarousel.trigger('next.owl.carousel');
        });
        projectSlider.owlCarousel({
            items: 4,
            dots: false,
            nav: false,
            autoplay: false,
            loop: true,
            autoWidth: true,
            margin: 20
        });
        galleryCarousel.owlCarousel({
            items: 1,
            dots: false,
            nav: false,
            autoplay: false,
            loop: true,
            margin: 20
        });
    }

    doc.addEventListener("DOMContentLoaded", function () {
        carousels();
    });
})(window, document);