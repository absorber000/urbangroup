<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/helpers/messengerLogger.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/helpers/leadgenRedmine.php');

$to  = 'pavel.ivanov@oknaokna.com';    // непосредственный адресат
$bcc = 'shtina@salemore.ru, lutsenko@salemore.ru';   // скрытая копия
$from = 'pavel.ivanov@oknaokna.com'; // адрес для ответа на письмо

$result = array(
    'success' => 'false',
    'mess' => 'Не удалось отправить!'
);

$subject = "Заявка на сайте окнаокна"; // Заголовок письма

$phone = !empty($_POST['phone']) ? $_POST['phone'] : '';
$name = !empty($_POST['name']) ? $_POST['name'] : '';
$formDesc = !empty($_POST['form_desc']) ? $_POST['form_desc'] : '';
$time = !empty($_POST['time']) ? $_POST['time'] : '';
$msg = !empty($_POST['message']) ? $_POST['message'] : '';
//echo '<pre>'; print_r($_SERVER); echo '</pre>';
if(
    $_SERVER['SERVER_NAME'] == 'oknaokna.com'
    || $_SERVER['SERVER_NAME'] == 'okna.salemore.ru'
) {
    $ticketUrl = \leadgenRedmine::createIssue($subject, $phone, $name, $time, $msg, $formDesc);
} else {$ticketUrl = 0;}

$telegramMsg = 'Имя: '.(!empty($name) ? $name : 'не указано')."\r\n".
    'Телефон: '.(!empty($phone) ? $phone : 'не указан')."\r\n";

if (isset($_POST['free_size'])) {
    include($_SERVER['DOCUMENT_ROOT'].'/mailSend/temp_free_size.php');

    $telegramMsg .= 'Время для звонка: '.(!empty($time) ? $time : 'не указано')."\r\n";
} else if(isset($_POST['application'])) {
    include($_SERVER['DOCUMENT_ROOT'].'/mailSend/temp_application.php');

    $telegramMsg .= 'Сообщение: '.(!empty($msg) ? $msg : 'не указано')."\r\n";
}

if (!empty($formDesc)) {
    $telegramMsg .= 'Форма: '.$formDesc."\r\n";
}
$telegramMsg .= 'Тикет: '.$ticketUrl."\r\n";

$host = !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']; 
$telegramMsg .= 'Cайт: '.$host;

if($_SERVER['SERVER_NAME'] == 'oknaokna.com') {
    \messengerLogger::sendToTelegram($subject . ': ' . "\r\n" . $telegramMsg);
}

//$message = 'Тело письма, которое отправлено для проверки!'; // тело письма

$headers  = "Content-type: text/html; charset=utf-8 \r\n";
$headers .= "From: oknaokna.com \r\n"; // От кого письмо
//$headers .= "=?utf-8?B?".base64_encode("окнаокна")."?=".$from."\r\n"; // От кого письмо
$headers .= "Reply-To: ".$from."\r\n"; // адрес для ответа на письмо
//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
$headers .= 'Bcc: '.$bcc. "\r\n";
if(
    $_SERVER['SERVER_NAME'] == 'oknaokna.com'
    || $_SERVER['SERVER_NAME'] == 'okna.salemore.ru'
) {
    $send = mail($to, $subject, $message, $headers);
} else {$send = true;}

if ($send === true) {
    $result = array(
        'success' => 'true',
        'mess' => 'Письмо успешно отправлено!',
		// '$ticketUrl' => $ticketUrl
    );
}

echo json_encode($result);