$(document).ready(function() {

    $('#write_message').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Поле 'Имя' обязательно к заполнению",
                minlength: "Введите не менее 2-х символов в поле 'Имя'"
            },
            phone: {
                required: "Поле 'Телефон' обязательно к заполнению"
            }
        }
    });

    $("#form1").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Поле 'Имя' обязательно к заполнению",
                minlength: "Введите не менее 2-х символов в поле 'Имя'"
            },
            phone: {
                required: "Поле 'Телефон' обязательно к заполнению"
            }
        }
    });

    $("#form2").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Поле 'Имя' обязательно к заполнению",
                minlength: "Введите не менее 2-х символов в поле 'Имя'"
            },
            phone: {
                required: "Поле 'Телефон' обязательно к заполнению"
            }
        }
    });

    $("#form3").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true
            },
            time: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Поле 'Имя' обязательно к заполнению",
                minlength: "Введите не менее 2-х символов в поле 'Имя'"
            },
            phone: {
                required: "Поле 'Телефон' обязательно к заполнению"
            },
            time: {
                required: "Поле 'Время' обязательно к заполнению"
            }
        }
    });

    $("#form4").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true
            },
            time: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Поле 'Имя' обязательно к заполнению",
                minlength: "Введите не менее 2-х символов в поле 'Имя'"
            },
            phone: {
                required: "Поле 'Телефон' обязательно к заполнению"
            },
            time: {
                required: "Поле 'Время' обязательно к заполнению"
            }
        }
    });


    var validate = $('#write_message').validate();
    var validate1 = $("#form1").validate();
    var validate2 = $("#form2").validate();
    var validate3 = $("#form3").validate();
    var validate4 = $("#form4").validate();
    var modalCallback = UIkit.modal("#modal-callback");
    var modalCallback2 = UIkit.modal("#modal-callback2");
    $("form").on("submit", function() {
        var form = $(this);
        if(
            validate.errorList.length > 0
            || validate1.errorList.length > 0
            || validate2.errorList.length > 0
            || validate3.errorList.length > 0
            || validate4.errorList.length > 0
            || selectRequired($(this).find('select.required')) == false
        )
            return false;

        var msg = $(this).serialize();
        mess = $(this).find('.mess');
        if (form.hasClass("modal__form")){
            var formContain = $(this).parents(".modal__inner");
            var sNotice = $(this).parents(".modal__inner").siblings(".modal__submit-notice");
        }
        else{
            var formContain = $(this).parents(".request-form__contain");
            var sNotice = formContain.siblings(".request-form__submit-notice");
        }
        mess.append('<img src="/mailSend/preloader.svg" class="preloader" />');

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: msg,
            success: function(data) {
                var res = $.parseJSON(data);

                if (res.success == 'true'){
                    mess.stop().hide(200, function(){
                        formContain.stop().slideUp(300);
                        sNotice.stop().slideDown(300, function(){
                            sNotice.addClass("visible");
                        })
                    });
                }
                else{
                    mess.html(res.mess);
                }
            },
            error:  function(xhr, str){
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });
        return false;
    });

    var elem = $(".recommendation__quote-item"),
        $cntSumm = $('.recommendation__quote-navigation-sum span'),
        $curSumm = $('.recommendation__quote-navigation-current')
    curCnt = 1,
        $next = $('.recommendation__quote-navigation-next'),
        $prev = $('.recommendation__quote-navigation-prev');

    $cntSumm.html(elem.length);

    $next.on('click', function() {
        if(curCnt == elem.length) curCnt = 0;
        curCnt+=1;
        $curSumm.html(curCnt);
    });
    $prev.on('click', function() {
        if(curCnt == 1) curCnt = elem.length+1;
        curCnt-=1;
        $curSumm.html(curCnt);
    });


    // добавить название формы в скрытое поле
    $(document).on('click', '.js-open-callback-popup-btn', function() {
        var name_form = $(this).attr('data-form-desc');
        console.log(name_form);
        $('div.uk-open input[name=form_desc]').val(name_form);
    });
});
function selectRequired(select) {
    var selectVal = select.val(),
        result = true,
        $error = $('<label class="error">Поле обязательно для заполнения</label>');


    if(selectVal == "") {
        result = false;
        select.next().after($error);
    }
    return result;
}