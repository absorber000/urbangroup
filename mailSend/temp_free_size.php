<?php
$name = $_POST['name'];
$phone = $_POST['phone'];
$time = $_POST['time'];
$formDesc = $_POST['form_desc'];

// тело письма
$message = '<b>Имя:</b> '.(!empty($name) ? $name : 'не указано').'</br>'.
           '<b>Телефон:</b> '.(!empty($phone) ? $phone : 'не указан').'</br>'.
           '<b>Время для звонка:</b> '.(!empty($time) ? $time : 'не указано');

if (!empty($ticketUrl)) {
    $message .= '</br><b>Тикет:</b> '.$ticketUrl;
}

if (!empty($formDesc)) {
    $message .= '</br><b>Форма:</b> '.$formDesc;
}
?>