"use strict";

;(function (win, doc) {
    'use strict';

    var carousels = {
        projectSlider: $("#projects-slider"),
        galleryCarousel: $("#gallery-slider"),
        galleryPrev: $("#gallery-slider-prev"),
        galleryNext: $("#gallery-slider-next"),
        sliderInitial: true,
        sliderExist: true,
        sliderProjCreate: function sliderProjCreate() {
            this.projectSlider.owlCarousel({
                items: 3,
                dots: false,
                nav: false,
                autoplay: false,
                loop: true,
                autoWidth: false,
                margin: 20
            });
        },
        init: function init() {
            var _this = this;

            this.galleryPrev.click(function () {
                _this.galleryCarousel.trigger('prev.owl.carousel');
            });
            this.galleryNext.click(function () {
                _this.galleryCarousel.trigger('next.owl.carousel');
            });
            this.sliderMake();
            this.galleryCarousel.owlCarousel({
                items: 1,
                dots: false,
                nav: false,
                autoplay: false,
                loop: true,
                margin: 20
            });
        },
        sliderMake: function sliderMake() {
            if (win.innerWidth <= 1199 && this.sliderExist) {
                this.projectSlider.trigger("destroy.owl.carousel");
                this.sliderExist = false;
                this.projectSlider.removeClass("owl-carousel");
            } else if (win.innerWidth >= 1200 && !this.sliderExist) {
                this.sliderProjCreate();
                this.sliderExist = true;
                this.projectSlider.addClass("owl-carousel");
            } else if (win.innerWidth >= 1200 && this.sliderInitial) {
                this.sliderProjCreate();
                this.sliderInitial = false;
                this.projectSlider.addClass("owl-carousel");
            }
        }
    };

    var menu = {
        $burger: $("#burger"),
        $menu: $("#menu"),
        $menuOverlay: $("#menu-overlay"),
        menuActive: false,
        openMenu: function openMenu() {
            var _this2 = this;

            this.$menu.addClass("menu--active");
            this.menuActive = true;
            this.$menuOverlay.stop().fadeIn(400, function () {
                _this2.$menuOverlay.addClass("menu-overlay--active");
            });
        },

        closeMenu: function closeMenu() {
            var _this3 = this;

            this.$menu.removeClass("menu--active");
            this.menuActive = false;
            this.$menuOverlay.stop().fadeOut(400, function () {
                _this3.$menuOverlay.removeClass("menu-overlay--active");
            });
        },

        init: function init() {
            var _this4 = this;

            this.$burger.click(function () {
                if (_this4.menuActive) {
                    _this4.closeMenu();
                } else if (!_this4.menuActive) {
                    _this4.openMenu();
                }
            });
            this.$menuOverlay.click(function () {
                if (_this4.menuActive) {
                    _this4.closeMenu();
                }
            });
        }
    };

    doc.addEventListener("DOMContentLoaded", function () {
        carousels.init();
        menu.init();
    });
    win.addEventListener("resize", function () {
        carousels.sliderMake();
    });
})(window, document);