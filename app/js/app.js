;(function(win, doc) {
    'use strict';

    const carousels = {
        projectSlider : $("#projects-slider"),
        galleryCarousel : $("#gallery-slider"),
        galleryPrev: $("#gallery-slider-prev"),
        galleryNext: $("#gallery-slider-next"),
        sliderInitial: true,
        sliderExist: true,
        sliderProjCreate: function(){
            this.projectSlider.owlCarousel({
                items: 3,
                dots: false,
                nav: false,
                autoplay: false,
                loop: true,
                autoWidth: false,
                margin: 20
            });
        },
        init: function() {
            this.galleryPrev.click(() => {
                this.galleryCarousel.trigger('prev.owl.carousel');
            });
            this.galleryNext.click(() => {
                this.galleryCarousel.trigger('next.owl.carousel');
            });
            this.sliderMake();
            this.galleryCarousel.owlCarousel({
                items: 1,
                dots: false,
                nav: false,
                autoplay: false,
                loop: true,
                margin: 20
            });
        },
        sliderMake: function(){
            if (win.innerWidth <= 1199 && this.sliderExist){
                this.projectSlider.trigger("destroy.owl.carousel");
                this.sliderExist = false;
                this.projectSlider.removeClass("owl-carousel");
            }
            else if (win.innerWidth >= 1200 && !this.sliderExist){
                this.sliderProjCreate();
                this.sliderExist = true;
                this.projectSlider.addClass("owl-carousel");
            }
            else if (win.innerWidth >= 1200 && this.sliderInitial){
                this.sliderProjCreate();
                this.sliderInitial = false;
                this.projectSlider.addClass("owl-carousel");
            }
        }
    };

    const menu = {
        $burger: $("#burger"),
        $menu: $("#menu"),
        $menuOverlay: $("#menu-overlay"),
        menuActive: false,
        openMenu: function(){
            this.$menu.addClass("menu--active");
            this.menuActive = true;
            this.$menuOverlay.stop().fadeIn(400, () => {
                this.$menuOverlay.addClass("menu-overlay--active");
            })
        },

        closeMenu: function(){
            this.$menu.removeClass("menu--active");
            this.menuActive = false;
            this.$menuOverlay.stop().fadeOut(400, () => {
                this.$menuOverlay.removeClass("menu-overlay--active");
            })
        },

        init: function(){
            this.$burger.click(() => {
                if (this.menuActive){
                    this.closeMenu();
                }
                else if (!this.menuActive){
                    this.openMenu();
                }
            });
            this.$menuOverlay.click(() => {
                if(this.menuActive){
                    this.closeMenu();
                }
            })
        }
    };

    doc.addEventListener("DOMContentLoaded", function() {
        carousels.init();
        menu.init();
    });
    win.addEventListener("resize", function() {
        carousels.sliderMake();
    });
})(window, document);