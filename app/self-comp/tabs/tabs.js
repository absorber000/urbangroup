;(function(win, doc) {
    'use strict';

    var Tabs = machina.Fsm.extend({
        initialize: function(el) {
            this.el = el.el;
            this.tabs = this.el.find(".js-tabs");
            this.tabsList = this.el.find(".js-tabs-list");
            this.tabsItem = this.el.find(".js-tabs-list>div");
            this.tabsContain= this.el.find(".js-tabs-contain");
            this.tabsContainList= this.el.find(".js-tabs-contain-list");
            this.tabsContainItem= this.el.find(".js-tabs-contain-list>div");
            this.tabsContainItem.addClass("js-tabs-content-item");
            this.tabsItem.addClass("js-tabs-item");
            $.each( this.tabsItem, ( key ) => {
                this.tabsItem.eq(key).attr('data-tab', key);
            });
            this.activeEl = 0;
            this.tabsItem.eq(0).addClass("js-tabs-item--active");
            this.tabsContainItem.eq(0).addClass("js-content-item-active");
            this.addEvents();
        },
        namespace: "tabs",

        initialState: "uninitialized",

        states: {
            uninitialized: {
                _onEnter: function() {
                    if (win.innerWidth > 768){
                        this.handle( "toTabs" );
                    }
                    if (win.innerWidth <= 768){
                        this.handle( "toAccordion" );
                    }
                },
                toTabs: "tabs",
                toAccordion: function(){
                    this.transition( "accordion" );
                    this.handle( "initialAccordion" );
                }
            },
            tabs:{
                _onEnter: function() {
                    this.emit( "sticky", { status: "unfixed" } );
                },
                initialAccordion: function(){
                    this.transition( "accordion" );
                    $.each( this.tabsItem, ( key ) => {
                        this.tabsContainItem.eq(key).insertAfter(this.tabsItem.eq(key));
                    });
                }
            },
            accordion:{
                _onEnter: function() {
                    this.emit( "sticky", { status: "unfixed" } );
                },
                initialTabs: function(){
                    this.transition( "tabs" );
                    $.each( this.tabsContainItem, ( key ) => {
                        this.tabsContainItem.eq(key).appendTo(this.tabsContainList);
                    });
                },
                initialAccordion: function(){
                    this.transition( "accordion" );
                    $.each( this.tabsItem, ( key ) => {
                        this.tabsContainItem.eq(key).insertAfter(this.tabsItem.eq(key));
                    });
                }
            }
        },

        addEvents: function(){
           const _this = this;
            this.tabsItem.click( function(){
                const attr = this.getAttribute("data-tab");
                if (attr !== _this.activeEl){
                    let prevActive = _this.activeEl;
                    _this.activeEl = attr;
                    if (_this.state === "accordion"){
                        _this.tabsItem.removeClass("js-tabs-item--active");
                        console.log(prevActive);
                        _this.tabsContainItem.eq(prevActive).slideUp(400, function(){
                            _this.tabsContainItem.eq(prevActive).removeClass("js-content-item-active");
                            _this.tabsContainItem.eq(attr).stop().slideDown(400, function(){
                                _this.tabsContainItem.eq(attr).addClass("js-content-item-active");
                            });
                        });
                        this.classList.add("js-tabs-item--active");
                    }
                    else if (_this.state === "tabs"){
                        _this.tabsItem.removeClass("js-tabs-item--active");
                        _this.tabsContainItem.removeClass("js-content-item-active");
                        _this.tabsContainItem.eq(attr).addClass("js-content-item-active");
                        this.classList.add("js-tabs-item--active");
                    }
                }
            });
            win.addEventListener('resize', event => this.resizeHandler());
        },

        findTransitionType: function(){
            switch(this.state){
                case "tabs":
                    if (win.innerWidth <= 768){
                        this.handle( "initialAccordion" );
                    }
                    break;
                case "accordion":
                    if (win.innerWidth > 768){
                        this.handle( "initialTabs" );
                    }
                    break;
            }
        },

        resizeHandler: function(){
            this.findTransitionType();
        }
    });

    doc.addEventListener("DOMContentLoaded", function() {
        var videoTabs = new Tabs({ el: $("#video-presentation-tabs") });
        console.log( videoTabs.compositeState() );
        videoTabs.on("transition", function (data){
            console.log(videoTabs.compositeState());
        });
    });
})(window, document);