;(function (win, doc) {
    'use strict';
    var map = document.getElementById('location-map');

    ymaps.ready(init);
    var myMap;

    function init() {
        myMap = new ymaps.Map("location-map", {
            center: [55.69020592114315, 37.42189067753947],
            zoom: 18,
            controls: ['zoomControl']
        });
        myMap.behaviors.disable('scrollZoom');
        if (window.innerWidth <= 768) {
            myMap.behaviors.disable('drag');
        }
        myMap.geoObjects
            .add(new ymaps.Placemark([55.69020592114315, 37.42189067753947], {
                balloonContent: 'ул. Рябиновая, 34а'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'images/map-marker.png',
                // Размеры метки.
                iconImageSize: [50, 60],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                // iconImageOffset: [-80, -254]
            }));
        window.addEventListener("resize", function () {
            myMap.container.fitToViewport();
        });
    }

})(window, document);



"use strict";

;(function (win, doc) {
    'use strict';

    function carousels() {
        var projectSlider = $("#projects-slider");
        var galleryCarousel = $("#gallery-slider");
        var galleryPrev = $("#gallery-slider-prev");
        var galleryNext = $("#gallery-slider-next");
        galleryPrev.click(function () {
            galleryCarousel.trigger('prev.owl.carousel');
        });
        galleryNext.click(function () {
            galleryCarousel.trigger('next.owl.carousel');
        });
        projectSlider.owlCarousel({
            items: 4,
            dots: false,
            nav: false,
            autoplay: false,
            loop: true,
            autoWidth: true,
            margin: 20
        });
        galleryCarousel.owlCarousel({
            items: 1,
            dots: false,
            nav: false,
            autoplay: false,
            loop: true,
            margin: 20
        });
    }

    doc.addEventListener("DOMContentLoaded", function () {
        carousels();
    });
})(window, document);