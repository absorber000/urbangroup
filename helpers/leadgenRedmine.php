<?

/*
 * Хелпер для работы с API Redmine
 */
class leadgenRedmine {

    const REDMINE_URL = 'http://redmine.salemore.ru';
    const API_KEY = 'f6c77c967f491bad5c956ae3e0fdc4e7d56f3232';
    const LEADGEN_PROJECT_ID = 48;
    const API_LEADGEN_USER_ID = 65;
    const LEAD_TRACKER_ID = 3;
    const NEW_STATUS_ID = 1;
    const PRIORITY_ID = 3;
    const SOURCE_LEAD_FIELD_ID = 13;
    const PHONE_FIELD_ID = 14;
    const NAME_FIELD_ID = 15;


    /**
     * @param $subject
     * @param $phone
     * @param string $name
     * @param string $time
     * @param string $message
     * @param null $formDesc
     * @return string
     */
    public static function createIssue($subject, $phone, $name = null, $time = null, $message = null, $formDesc = null) {
        $name = !empty($name) ? $name : 'не указано';
        $time = !empty($time) ? $time : 'не указано';
        $message = !empty($message) ? $message : 'не указано';

        $description = '*Имя:* '.$_POST['name']."\r\n".
                       '*Телефон:* '.$phone."\r\n".
                       '*Сообщение:* '.$message."\r\n".
                       '*Время для звонка:* '.$time."\r\n".
                       '*Дата создания заявки:* '.date('d.m.Y').' в '.date('H:i');

        if (!empty($formDesc)) {
            $description .= "\r\n".'*Форма:* '.$formDesc;
        }
        $content = json_encode(array('issue' => array(
            'project_id' => \leadgenRedmine::LEADGEN_PROJECT_ID,
            'tracker_id' => \leadgenRedmine::LEAD_TRACKER_ID,
            'status_id' => $_REQUEST['SERVER_NAME'] == 'oknaokna.com' ? \leadgenRedmine::NEW_STATUS_ID : 4,
            'assigned_to_id' => \leadgenRedmine::API_LEADGEN_USER_ID,
            'priority_id' => \leadgenRedmine::PRIORITY_ID,
            'subject' => $subject,
            'description' => $description,
            'custom_fields' => array(
                array('id' => \leadgenRedmine::NAME_FIELD_ID, 'value' => $name),
                array('id' => \leadgenRedmine::PHONE_FIELD_ID, 'value' => $phone),
                array('id' => \leadgenRedmine::SOURCE_LEAD_FIELD_ID, 'value' => 'Обратный звонок'),
            ),
        )));

        $headers = array(
            'Content-type: application/json',
            'Content-length: '.mb_strlen($content),
            'X-Redmine-API-Key: '.\leadgenRedmine::API_KEY,
        );

        /* $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $headers)."\r\n",
                'content' => $content,
            ),
        )); */

        // return $response = json_decode(file_get_contents(\leadgenRedmine::REDMINE_URL.'/issues.json', false, $context));
		$response = leadgenRedmine::_curlRequest(\leadgenRedmine::REDMINE_URL.'/issues.json', $content, $headers);

		if (!empty($response['error'])) {
			$ticketUrl = 'ошибка создания лида в redmine: '.$response['error'];
		} else {			
		
			if (!empty($response['data'])) {
				$response = json_decode($response['data']);
				
				if (!empty($response->issue) && !empty($response->issue->id)) {
					$ticketUrl = \leadgenRedmine::REDMINE_URL.'/issues/'.$response->issue->id.'/';
				} else {
					$ticketUrl = 'ошибка создания лида в redmine';
				}
			}
		}
		
        return $ticketUrl;
    }
	
	public static function _curlRequest($url, $params = null, $headers = array(), $isPost = true) {
        $curlObject = curl_init($url);
		
		if (!empty($headers)) {
			curl_setopt($curlObject, CURLOPT_HTTPHEADER, $headers);
		}
		
        if ($isPost) {
            curl_setopt($curlObject, CURLOPT_POST, 1);
        }

        curl_setopt($curlObject, CURLOPT_HEADER, 0);
        curl_setopt($curlObject, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlObject, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curlObject, CURLOPT_SSL_VERIFYPEER, 0);

        if (!empty($params)) {
            curl_setopt($curlObject, CURLOPT_POSTFIELDS, $params);
        }

        // Выполняем запрос
        $response = curl_exec($curlObject);
        $curlErrorMsg = curl_error($curlObject);
        $error = null;

        if (!empty($curlErrorMsg)) {
            $response = null;
            $error = $curlErrorMsg;
        } else {
            $responseHttpCode = curl_getinfo($curlObject, CURLINFO_HTTP_CODE);

            if ($responseHttpCode != 201 && $responseHttpCode != 200 ) {
                $response = null;
                $error = 'Неожиданный код HTTP: '.$responseHttpCode;
            }
        }
        curl_close($curlObject);

        return array(
            'data' => $response,
            'error' => $error,
        );
    }
}