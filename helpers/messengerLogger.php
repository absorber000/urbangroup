<?

/*
 * Хелпер для отправки сообщений о различных событиях в мессенджеры
 */
class messengerLogger {

    const TELEGRAM_SENDER_API_URL = 'http://api.bot.brotkin.ru/api/messages/';
    const TELEGRAM_BOT_CODE = 'ff8075c178c947ce9b52fd7a812c6540';
    const TELEGRAM_PRODUCT_CHAT_BOT_RULE_ID = '40';
    /**
     * Отправка сообщения в чат Telegram
     *
     * @param string $message Текст сообщения
     */
    public static function sendToTelegram($message) {
        file_get_contents(
            \messengerLogger::TELEGRAM_SENDER_API_URL.'?'.http_build_query(array(
                'rule' => \messengerLogger::TELEGRAM_PRODUCT_CHAT_BOT_RULE_ID,
                'code' => \messengerLogger::TELEGRAM_BOT_CODE,
                'message' => $message,
            ))
        );
    }
}